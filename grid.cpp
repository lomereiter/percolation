#include "grid_square.hpp"
#include "grid_hexagonal.hpp"

template class Node<Square>;
template class Grid<Square>;
template class Node<Hexagonal>;
template class Grid<Hexagonal>;

extern "C" SEXP get_threshold_square(SEXP _n) {
    return get_threshold<Square>(_n);
}

extern "C" SEXP get_threshold_hexagonal(SEXP _n) {
    return get_threshold<Hexagonal>(_n);
}
