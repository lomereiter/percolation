#ifndef GRID_HEXAGONAL_HPP
#define GRID_HEXAGONAL_HPP

#include "grid_impl.hpp"

template <>
size_t Node<Hexagonal>::num() const {
    /*
     *     // <--- border
     *
     *       0 -- 1        2 -- 3             i = 0
     *     //      \      /      \\
     *     4        5 -- 6        7           i = 1
     *     \\      /      \      //
     *       8 -- 9        10 - 11            i = 2
     *     //      \      /      \\
     *     12       13 - 14      15           i = 3
     *
     *      0      1      2      3   <-- j
     */
    return i * N + j;
}

template <>
class Node<Hexagonal>::possible_neighbour_range {
    const Node<Hexagonal>& node;
    size_t i;
    Node<Hexagonal> current_node;
    int dx[3];
    int dy[3];
public:
    possible_neighbour_range(const Node<Hexagonal>& node) : 
        node(node), i(0), current_node(node.grid, node.i + dx[0], node.j + dy[0], 0)
    {
        dx[0] = 0;
        if (0 == (node.i + node.j) % 2) {
            dy[0] = 1;
        } else {
            dy[0] = -1;
        }
        dx[1] = 1;  dy[1] = 0;
        dx[2] = -1; dy[2] = 0;
    }

    bool empty() const {
        return i == 3;
    }

    void pop_front() {
        ++i;
        current_node = node.grid->node(node.i + dx[i], node.j + dy[i], 0);
    }

    const Node<Hexagonal>& front() const {
        return current_node;
    }
};

template <>
size_t Grid<Hexagonal>::initial_number_of_nodes(size_t N) {
    return N * N;
}

template <>
bool Grid<Hexagonal>::initially_contains(const Node<Hexagonal>& node) const {
    return node.x_coord() < N && node.y_coord() < N;
}

template <>
class Grid<Hexagonal>::left_border_range {
    const Grid<Hexagonal>* g;
    size_t i;
    Node<Hexagonal> node;
    void next() {
        for ( ; i < g->size(); i++) {
            node = Node<Hexagonal>(g, i, 0, 0);
            if (node.is_valid()) {
                break;
            }
        }
    }
public:
    left_border_range(const Grid<Hexagonal>* g) : g(g), i(0) {
        next();
    }
    
    bool empty() const { return i >= g->size(); }
    const Node<Hexagonal>& front() const { return node; }
    void pop_front() { i++; next(); }
};

template <>
bool Grid<Hexagonal>::is_right_border_node(const Node<Hexagonal>& node) const {
    return node.y_coord() == N - 1;
}

#endif
