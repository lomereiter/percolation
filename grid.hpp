#ifndef GRID_HPP
#define GRID_HPP

#include <R.h>

//#define USE_INTERNALS

#include <Rdefines.h>
#include <Rinternals.h>

#include <cstddef> // size_t
#include <vector>

template <typename T> class Grid;

template <typename T>
class Node {
    const Grid<T>* grid; // the grid to which the node belongs
    size_t i; // stores x coord
    size_t j; // stores y coord
    size_t k; // stores z coord

    size_t N; // stores grid size to be able to compute num()

public:
    Node() {}
    Node(const Grid<T>* g, size_t i, size_t j, size_t k);

    size_t x_coord() const; // getters for i, j, k
    size_t y_coord() const; 
    size_t z_coord() const;

    size_t num() const; // returns unique node identifier (w/ respect to its grid)

	struct possible_neighbour_range {
		bool empty() const;
		void pop_front();
		const Node<T>& front() const;
	    possible_neighbour_range(const Node&);
	};

	class neighbour_range {
		possible_neighbour_range range;
	public:
		bool empty() const;
		void pop_front();
		const Node<T>& front() const;
		neighbour_range(const Node&);
	};

    Node<T>::neighbour_range neighbours() const;

    bool is_valid() const;
};

template <typename GridType>
class Grid {
    const size_t N;
    std::vector<int> arr; // is the node enabled?

    const int* remove_sequence; // sequence of indices of nodes to remove
public:
    typedef GridType type_t;
    typedef Node<GridType> node_t;

    Grid (size_t n);
    ~Grid();

    void update(size_t to_remove); 

    size_t size() const; // getter for N

    bool initially_contains(const node_t&) const;
    bool node_is_enabled(const node_t&) const;

    size_t initial_number_of_nodes() const;
    static size_t initial_number_of_nodes(size_t N);

	struct left_border_range {
		const node_t& front() const;
		bool empty() const;
		void pop_front();
		left_border_range(const Grid*);
	};

    left_border_range left_border_nodes() const;
    bool is_right_border_node(const node_t& node) const;

    node_t node(size_t i, size_t j, size_t k) const;

    bool is_connected() const;
};

struct Square {};
struct Hexagonal {};

template <typename T> static SEXP get_threshold(SEXP n);
#endif
