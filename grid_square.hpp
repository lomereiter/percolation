#ifndef GRID_SQUARE_HPP
#define GRID_SQUARE_HPP

#include "grid_impl.hpp"

template <>
inline size_t Node<Square>::num() const {
    /*
     *   0 -- 1 -- 2    i = 0
     *  ||    |    || 
     *   3 -- 4 -- 5    i = 1
     *  ||    |    || 
     *   6 -- 7 -- 8    i = 2
     */
    return i * N + j;
}


template <>
class Node<Square>::possible_neighbour_range {
    static int dx[];
    static int dy[];
    size_t index;
    const Node<Square>& node;
    Node<Square> current_node;
public:
    possible_neighbour_range(const Node<Square>& node) : 
        index(0), node(node), 
        current_node(node.grid, node.i + dx[0], node.j + dy[0], 0)
    {
    }

    bool empty() const { return index >= 4; }
    const Node<Square>& front() const { return current_node; }
    void pop_front() {
        ++index;
        current_node = node.grid->node(node.i + dx[index],
                                       node.j + dy[index], 0);
    }
};

int Node<Square>::possible_neighbour_range::dx[] = {1,-1,0,0};
int Node<Square>::possible_neighbour_range::dy[] = {0,0,-1,1};

template <>
inline size_t Grid<Square>::initial_number_of_nodes(size_t N) {
    return N * N;
}

template <>
inline bool Grid<Square>::initially_contains(const Node<Square>& node) const {
    return (node.x_coord() < N && node.y_coord() < N);
}

template <>
class Grid<Square>::left_border_range {
    const Grid<Square>* g;
    size_t i;
    Node<Square> node;
    void next() {
        for ( ; i < g->size(); i++) {
            node = Node<Square>(g, i, 0, 0);
            if (node.is_valid()) {
                break;
            }
        }
    }
public:
    left_border_range(const Grid<Square>* g) : g(g), i(0) {
        next();
    }
    
    bool empty() const { return i >= g->size(); }
    const Node<Square>& front() const { return node; }
    void pop_front() { i++; next(); }
};

template <>
inline bool Grid<Square>::is_right_border_node(const Node<Square>& node) const {
    return node.y_coord() == N - 1;
}
#endif
