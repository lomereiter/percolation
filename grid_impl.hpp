#ifndef GRID_IMPL_HPP
#define GRID_IMPL_HPP

#include "grid.hpp"

#include <queue>
#include <algorithm> // std::fill

template <typename T>
inline Node<T>::Node(const Grid<T>* g, size_t i, size_t j, size_t k) : 
    grid(g), i(i), j(j), k(k), N(grid->size()) 
{
}

template <typename T>
inline bool Node<T>::is_valid() const {
    return grid->initially_contains(*this) && grid->node_is_enabled(*this);
}

template <typename T>
inline typename Node<T>::neighbour_range Node<T>::neighbours () const {
    return typename Node<T>::neighbour_range(*this);
}

template <typename T> inline size_t Node<T>::x_coord() const { return i; }
template <typename T> inline size_t Node<T>::y_coord() const { return j; }
template <typename T> inline size_t Node<T>::z_coord() const { return k; }

template <typename T>
inline Node<T> Grid<T>::node(size_t i, size_t j, size_t k) const {
    return Node<T>(this, i, j, k);
}

template <typename T>
inline Node<T>::neighbour_range::neighbour_range(const Node& node) : 
    range(possible_neighbour_range(node)) 
{
	while (!range.empty() && !range.front().is_valid())
        range.pop_front();
}

template <typename T>
inline const Node<T>& Node<T>::neighbour_range::front() const {
	return range.front();
}

template <typename T>
inline bool Node<T>::neighbour_range::empty() const {
	return range.empty();
}

template <typename T>
inline void Node<T>::neighbour_range::pop_front() {
	range.pop_front();
	while (!range.empty() && !range.front().is_valid())
        range.pop_front();
}

static SEXP zero_indexed_sample(SEXP);
template <typename T>
inline Grid<T>::Grid (size_t n) : N(n), arr(initial_number_of_nodes(n), 1)
{
    SEXP remove_sequence_sexp, remove_sequence_len;
    PROTECT(remove_sequence_len = ScalarInteger(arr.size()));
    PROTECT(remove_sequence_sexp = zero_indexed_sample(remove_sequence_len));

    remove_sequence = INTEGER(remove_sequence_sexp);
}

template <typename T>
inline Grid<T>::~Grid() {
    UNPROTECT(2);
}

template <typename T>
inline void Grid<T>::update(size_t to_remove) {
    std::fill (arr.begin(), arr.end(), 1);
    for (size_t i = 0; i < to_remove; ++i) {
        arr[remove_sequence[i]] = 0;
    }
}

template <typename T>
inline size_t Grid<T>::size() const { 
    return N; 
}

template <typename T>
inline bool Grid<T>::is_connected () const {
    const size_t BEGIN = initial_number_of_nodes();
    const size_t END = BEGIN + 1;

    typedef Node<T> node_t;

    std::vector<int> visited (END + 1, 0);
    std::queue<node_t> to_be_visited;

    for (left_border_range range = left_border_nodes();
         !range.empty(); range.pop_front())
    {
        to_be_visited.push(range.front());
        visited[range.front().num()] = 1;
    }

    while (!to_be_visited.empty()) {
        node_t next = to_be_visited.front();
        to_be_visited.pop();
        typename Node<T>::neighbour_range neighbours = next.neighbours();
        for ( ; !neighbours.empty(); neighbours.pop_front()) {
            node_t v = neighbours.front();
            if (!visited[v.num()]) {
                if (is_right_border_node(v)) {
                    return true;
                }
                visited[v.num()] = 1;
                to_be_visited.push(v);
            }
        }
    }
    return false;
}

template <typename T>
inline size_t Grid<T>::initial_number_of_nodes() const {
    return Grid<T>::initial_number_of_nodes(N);
}

template <typename T>
inline bool Grid<T>::node_is_enabled(const Node<T>& node) const {
    return static_cast<bool>(arr[node.num()]);
}

template <typename T>
inline typename Grid<T>::left_border_range Grid<T>::left_border_nodes() const {
	return left_border_range(this);
}

inline static SEXP zero_indexed_sample(SEXP n) {
    SEXP sample, minus, e, e1, result, one;
   
    PROTECT(e1 = allocVector(LANGSXP, 3));

        PROTECT(minus = findFun(install("-"), R_GlobalEnv));
        SETCAR(e1, minus);
        
        PROTECT(sample = findFun(install("sample.int"), R_BaseEnv));
        PROTECT(e = allocVector(LANGSXP, 2));
        SETCAR(e, sample);
        SETCADR(e, n);
        SETCADR(e1, e);

        PROTECT(one = ScalarInteger(1));
        SETCADDR(e1, one);

    PROTECT(result = eval(e1, R_BaseEnv));
    UNPROTECT(6);
    return result;
}

template <typename T>
static SEXP get_threshold(SEXP _n) {
    int n = INTEGER_VALUE(_n);

    Grid<T> grid(n);
    
    size_t l = 1;

    size_t r = grid.initial_number_of_nodes();

    size_t m;

    while (l != r - 1) {
        m = (l + r) / 2;
        grid.update(m);
        if (grid.is_connected()) {
            l = m;
        } else {
            r = m;
        }
    }

    SEXP result;
    PROTECT(result = ScalarInteger(grid.initial_number_of_nodes() - r));
    UNPROTECT(1);
    return result;
}
#endif
